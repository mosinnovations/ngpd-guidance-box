# Mercer Financial Wellness Development Box

Development box for the MFW project, provisioned with Ansible, Docker and Vagrant.

## Install Instructions

1. Download and install [Vagrant](https://www.vagrantup.com/downloads.html)
1. Download and install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
1. Clone this repository `git clone git@bitbucket.org:mosinnovations/mfw-box.git`
1. Cd into directory `NGPD-GUIDANCE-BOX`
1. Type `vagrant up`
1. Type `vagrant provision`
1. Wait for the box to provision, the script will exit once provision is successful.
1. `vagrant ssh` to log onto the box
1. `cd ngpd-guidance-master` and run `sudo npm start`
1. Open browser winow and navigate to `http://localhost:3000` to access the UI
1. Start configuring a project
1. Create a model and set the data source url to `http://localhost:7000/employee_runpath?_start=0&_limit=100`

### PORTS

3000: UI
3100: API
7000: Mock server
4300: Mongo
4400: redis

## Service Concepts